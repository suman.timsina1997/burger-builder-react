import axios from 'axios';


const instance = axios.create({
    baseURL:'https://react-my-burger-7e3e6.firebaseio.com/'
});


export default instance;