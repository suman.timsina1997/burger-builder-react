import React from 'react';
import {Button } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMobile , faUser } from '@fortawesome/free-solid-svg-icons'
import classes from './Order.module.css';

const order = (props) =>{
    const ingredients= [];
    const orderedBy = [];

    for(let ingredientName in props.ingredients){
        ingredients.push({
            name: ingredientName,
            amount:props.ingredients[ingredientName]})
    };
    for(let orderData in props.orderedBy){
        orderedBy.push({
            name: orderData,
            value:props.price[orderData]})
    };
    console.log(props.orderedBy);


    const outputIngredient= ingredients.map(ig=>{
    return <span 
        style={{
            textTransform:"capitalize",
            display:'inline-block',
            margin: '0 8px',
            border:'1px solid #ccc',
            padding:'5px'
        }}
        key={ig.name}>{ig.name} ({ig.amount})</span>
    })

    return(
            <div className={classes.Order}>
                <p>Order Ingregients:{outputIngredient}</p>
                <p>Price:&nbsp;<strong>{props.price}</strong></p>
                <p><strong>Ordered By:</strong></p>
                <p><FontAwesomeIcon icon={faUser} /> : {props.orderedBy.name}</p>
                <p><FontAwesomeIcon icon={faMobile} /> : {props.orderedBy.phone}</p>
                <Button color="danger">Delete Order</Button>

            </div>);
    };

export default order;


