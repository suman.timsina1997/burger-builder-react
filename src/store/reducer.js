import * as actionType from './actions.js';

const initialState={
    ingrediants:{
        salad:0,
        bacon:0,
        cheese:0,
        meat:0
    },
    totalPrice: 400,
};
const INGREDIANTS_PRICE={
    salad: 50,
    meat:130,
    cheese :70,
    bacon:30,

};

const reducer = (state=initialState, action)=>{
    switch(action.type){
        case actionType.ADD_INGREDIENT:
            return{
                ...state,
                ingrediants:{
                    ...state.ingrediants,
                    [action.ingredientName]:state.ingrediants[action.ingredientName] + 1

                },
                totalPrice:state.totalPrice + INGREDIANTS_PRICE[action.ingredientName]
            }
        case actionType.REMOVE_INGREDIENT:
            return{
                ...state,
                ingrediants:{
                    ...state.ingrediants,
                    [action.ingredientName]:state.ingrediants[action.ingredientName] - 1

                },
                totalPrice:state.totalPrice - INGREDIANTS_PRICE[action.ingredientName]
            }
        default:
            return state;
        
    }
};

export default reducer;