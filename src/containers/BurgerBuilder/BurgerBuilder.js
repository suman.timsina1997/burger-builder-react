import React ,{Component} from 'react';
import {connect} from 'react-redux';
import Aux from '../../hoc/Auxiliary/Auxiliary';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import Spinner from '../../components/UI/Spinner/Spinner';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import axios from '../../axios-orders';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import * as actionType from '../../store/actions';


class BurgerBuilder extends Component{
    state= {
        purchasable : false,
        purchasing : false,
        loading: false,
        error:false,
    }

    componentDidMount(){
        // console.log(this.props);
        // axios.get('https://react-my-burger-7e3e6.firebaseio.com/ingredients.json')
        // .then(response=>{
        //     this.setState({ingrediants:response.data})
        // })
        // .catch(error=>{
        //     this.setState({error:true})
        // });
    }


    updatePurchaseState (ingrediants){
        const sum = Object.keys(ingrediants).map(igKey=>{
            return ingrediants[igKey];
        }).reduce((sum , el) => {
            return sum + el;
        },0);
        return  sum>0;
    }


    purchaseHandler= () => {
        this.setState({purchasing:true});
    };

    purchaseCancelHandler = () =>{
        this.setState({purchasing:false});
    };

    purchaseContinueHandler = ()=>{
        this.props.history.push({
            pathname: '/checkout'});
    };

    render (){
        const disableInfo = {
            ...this.props.ings
        };
        for(let key in disableInfo){
            disableInfo[key]= disableInfo[key] <= 0;
        }

        let orderSummary=null;
        
        if(this.state.loading){
            orderSummary=<Spinner/>
        }
        
        let burger =this.state.error ? <p>Ingredients can't fetched </p>: <Spinner/>
        if(this.props.ings){
            burger= (
                <Aux>
                    <Burger ingrediants={this.props.ings} />
                    <BuildControls 
                    ingrediantAdded={this.props.onIngredientAdded}
                    ingredientRemoved= {this.props.onIngredientRemoved}
                    disabled={disableInfo}
                    purchasable ={ this.updatePurchaseState(this.props.ings)}
                    ordered={this.purchaseHandler}
                    price={this.props.price}
                    />
                </Aux>
            );
            orderSummary=<OrderSummary ingredients={this.props.ings }
                        price={this.props.price}
                        purchaseCanclled={this.purchaseCancelHandler}
                        purchaseContinued={this.purchaseContinueHandler}
                        />
        }

       


        return (
            <Aux>
                <Modal show={this.state.purchasing} modalClosed={this.purchaseCancelHandler}>
                   {orderSummary}
                </Modal>
                {burger}
            </Aux>
        );
    }
};


const mapStateToProps = state =>{
    return{
        ings:state.ingrediants,
        price:state.totalPrice
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        onIngredientAdded : (ingsName)=> dispatch({type: actionType.ADD_INGREDIENT , ingredientName:ingsName}),
        onIngredientRemoved : (ingsName)=> dispatch({type: actionType.REMOVE_INGREDIENT , ingredientName:ingsName}),

    }
}

export default connect(mapStateToProps,mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios));