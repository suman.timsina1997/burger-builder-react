import React, {Component} from 'react';
import classes from './Layout.module.css';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';
import Aux from '../Auxiliary/Auxiliary';

class Layout extends Component{
    state= {
        SideDrawerShow: false
    }

    SideDrawerClosedHandler = () => {
        this.setState({ SideDrawerShow:false})
    }
    SideDrawerToggleHandler = () => {
        this.setState((prevState) => {
            return { SideDrawerShow: !prevState.SideDrawerShow };
        });
    }


    render() {
        return (
            <Aux>
                <Toolbar drawerToggleClicked={this.SideDrawerToggleHandler}/>
                <SideDrawer
                    open={this.state.SideDrawerShow}
                    closed={this.SideDrawerClosedHandler} />
                <main className={classes.Content}>
                    {this.props.children}
                </main>
            </Aux>
        )
    }
};

export default Layout